create database hackathon;
use hackathon;
create table stock(
    id int primary key auto_increment,
stockTicker varchar (30),
stockName varchar(50),
price double,
volume int,
buyOrSell  varchar(30),
statusCodecity int,
    date_time DATETIME DEFAULT CURRENT_TIMESTAMP
);
select * from stock;
insert into stock (id,stockTicker,stockName,price,volume,buyOrSell,statusCodeCity) values(1,'C','Citigroup Inc.',100,10,'buy',0);
insert into stock (id,stockTicker,stockName,price,volume,buyOrSell,statusCodeCity) values(2,'AAPL','Apple Inc.',500,15,'sell',1);
insert into stock (id,stockTicker,stockName,price,volume,buyOrSell,statusCodeCity) values(3,'NFLX','Netflix Inc.',600,100,'buy',2);
insert into stock (id,stockTicker,stockName,price,volume,buyOrSell,statusCodeCity) values(4,'WFC','Wells Fargo',50,60,'sell',1);
insert into stock (id,stockTicker,stockName,price,volume,buyOrSell,statusCodeCity) values(5,'JPM','J.P.Morgon',400,500,'buy',2);
insert into stock (id,stockTicker,stockName,price,volume,buyOrSell,statusCodeCity) values(6,'AMZN','Amazon Inc.'900,500,'buy',2);